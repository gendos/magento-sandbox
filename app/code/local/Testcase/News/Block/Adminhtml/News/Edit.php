<?php
/**
 * Created by PhpStorm.
 * User: gendos
 * Date: 27.11.16
 * Time: 13:54
 */
class Testcase_News_Block_Adminhtml_News_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'testcasenews';
        $this->_controller = 'adminhtml_news';

        $this->_addButton('save_and_continue', [
            'label' => Mage::helper('testcasenews')->__('Save and Continue Edit'),
            'onclick' => 'saveAndContinueEdit(\''.$this->getSaveAndContinueUrl().'\')',
            'class' => 'save',
        ], -100);
        $this->_formScripts[] = "function saveAndContinueEdit(){ editForm.submit($('edit_form').action + 'back/edit/'); }";
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('testcasenews');
        $model = Mage::registry('current_news');

        if ($model->getId()) {
            return $helper->__("Edit News item '%s'", $this->escapeHtml($model->getTitle()));
        } else {
            return $helper->__("Add News item");
        }
    }

    public function getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', [
            '_current'   => true,
            'back'       => 'edit',
            'active_tab' => null,
        ]);
    }
}