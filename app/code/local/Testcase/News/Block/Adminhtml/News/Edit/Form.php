<?php
/**
 * Created by PhpStorm.
 * User: gendos
 * Date: 27.11.16
 * Time: 13:56
 */
class Testcase_News_Block_Adminhtml_News_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $helper = Mage::helper('testcasenews');
        $model = Mage::registry('current_news');

        $form = new Varien_Data_Form([
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', [
                'id' => $this->getRequest()->getParam('id'),
            ]),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ]);

        $this->setForm($form);

        $fieldset = $form->addFieldset('news_form', ['legend' => $helper->__('News Information')]);

        $fieldset->addField('title', 'text', [
            'label' => $helper->__('Title'),
            'required' => true,
            'name' => 'title',
        ]);
        $fieldset->addField('status', 'select', [
            'label' => $helper->__('Status'),
            'name' => 'status',
            'values' => [
                [ 'value' => 1, 'label' => $helper->__('Enabled') ],
                [ 'value' => 0, 'label' => $helper->__('Disabled') ],
            ],
        ]);
        $fieldset->addField('content', 'editor', [
            'label' => $helper->__('Content'),
            'required' => true,
            'name' => 'content',
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            'wysiwyg' => true,
        ]);

        $form->setUseContainer(true);

        if($data = Mage::getSingleton('adminhtml/session')->getFormData()){
            $form->setValues($data);
        } else {
            $form->setValues($model->getData());
        }

        return parent::_prepareForm();
    }

}