<?php
/**
 * Created by PhpStorm.
 * User: gendos
 * Date: 27.11.16
 * Time: 12:28
 */

class Testcase_News_Block_Adminhtml_News_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('testcasenews/news')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('testcasenews');

        $this->addColumn('news_id', [
            'header' => $helper->__('ID'),
            'index' => 'news_id',
            'width' => '50px',
        ]);
        $this->addColumn('title', [
            'header' => $helper->__('Title'),
            'index' => 'title',
            'type' => 'text',
        ]);
        $this->addColumn('status', [
            'header' => $helper->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => $this->getEnableDisableValues(),
        ]);
        $this->addColumn('date_created', [
            'header' => $helper->__('Created'),
            'index' => 'date_created',
            'type' => 'datetime',
        ]);
        $this->addColumn('date_updated', [
            'header' => $helper->__('Updated'),
            'index' => 'date_update',
            'type' => 'datetime',
        ]);

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $helper = Mage::helper('testcasenews');

        $this->setMassactionIdField('news_id');
        $this->getMassactionBlock()->setFormFieldName('news');

        $this->getMassactionBlock()->addItem('delete', [
            'label' => $helper->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ]);
        $this->getMassactionBlock()->addItem('status', [
            'label'=> $helper->__('Change status'),
            'url'  => $this->getUrl('*/*/massStatus', ['_current'=>true]),
            'additional' => [
                'visibility' => [
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => $helper->__('Status'),
                    'values' => $this->getEnableDisableValues(),
                ],
            ],
        ]);

        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', [
            'id' => $row->getId()
        ]);
    }

    public function getEnableDisableValues()
    {
        return [
            1 => Mage::helper('testcasenews')->__('Enabled'),
            0 => Mage::helper('testcasenews')->__('Disabled'),
        ];
    }
}