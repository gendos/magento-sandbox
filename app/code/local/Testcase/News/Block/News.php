<?php
/**
 * Created by PhpStorm.
 * User: gendos
 * Date: 27.11.16
 * Time: 12:02
 */

class Testcase_News_Block_News extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        /* @var Mage_Page_Block_Html_Pager $pager */
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(['none']);
        $pager->setLimit(Mage::helper('testcasenews/config')->getPageNewsCount());
        $pager->setPageVarName('page');
        $pager->setShowAmounts(false);
        $this->setChild('pager', $pager);
        $pager->setCollection($this->getCollection());

        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getCollection()
    {
        /* @var Testcase_News_Model_Resource_News_Collection $newsCollection */
        $newsCollection = Mage::getModel('testcasenews/news')
            ->getCollection()
            ->setOrder('date_created', 'DESC')
            ->addFilter('status', 1);

        return $newsCollection;
    }

    public function getCurrentPageCollection()
    {
        return $this->getChild('pager')->getCollection();
    }
}