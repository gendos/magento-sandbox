<?php
/**
 * Created by PhpStorm.
 * User: gendos
 * Date: 27.11.16
 * Time: 17:13
 */
class Testcase_News_Model_Config_NewsCount
{
    private static $values = [
        5, 10, 20, 50, 100,
    ];

    public function toOptionArray()
    {
        $result = [];

        foreach (static::$values as $value)
            $result[] = ['value' => $value, 'label' => $value];

        return $result;
    }
}