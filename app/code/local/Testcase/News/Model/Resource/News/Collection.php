<?php
/**
 * Created by PhpStorm.
 * User: gendos
 * Date: 27.11.16
 * Time: 01:20
 */


class Testcase_News_Model_Resource_News_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('testcasenews/news');
    }

}