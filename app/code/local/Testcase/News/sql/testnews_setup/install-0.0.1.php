<?php
/**
 * Created by PhpStorm.
 * User: gendos
 * Date: 26.11.16
 * Time: 16:50
 */
/* @var Mage_Core_Model_Resource_Setup $this  */

$installer = $this;

$tableName = $installer->getTable('testcasenews_resource/table_news');

$installer->startSetup();

/* @var Varien_Db_Adapter_Interface $connection */
$connection = $installer->getConnection();

$connection->dropTable($tableName);
$table = $connection->newTable($tableName)
    ->addColumn('news_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, [
        'identity'  => true,
        'primary'   => true,
        'nullable'  => false,
    ])
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, '255', [
        'nullable'  => false,
    ])
    ->addColumn('content', Varien_Db_Ddl_Table::TYPE_TEXT, null, [
        'nullable'  => false,
    ])
    ->addColumn('date_created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, [
        'nullable'  => false,
        //mysql 5.5 doesn't allow to create multiple timestamps with default
        'default' => 0, //Varien_Db_Ddl_Table::TIMESTAMP_INIT
    ])
    ->addColumn('date_modified', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, [
        'nullable'  => false,
        'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE
    ])
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_BOOLEAN, null, [
        'nullable'  => false,
        'default' => 0,
    ]);
$connection->createTable($table);

$installer->endSetup();