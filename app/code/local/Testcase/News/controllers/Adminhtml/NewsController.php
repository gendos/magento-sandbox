<?php
/**
 * Created by PhpStorm.
 * User: gendos
 * Date: 27.11.16
 * Time: 12:16
 */
class Testcase_News_Adminhtml_NewsController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('testcasenews');

        $contentBlock = $this->getLayout()->createBlock('testcasenews/adminhtml_news');
        $this->_addContent($contentBlock);
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_news', Mage::getModel('testcasenews/news')->load($id));

        $this->loadLayout()->_setActiveMenu('testcasenews');
        $this->_addContent($this->getLayout()->createBlock('testcasenews/adminhtml_news_edit'));
        $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        $this->renderLayout();
    }


    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('testcasenews/news');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));
                if(!$model->getData('date_created')){
                    $model->setData('date_created', now());
                }
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('News was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back') == 'edit') {
                    $this->_redirect('*/*/edit', [
                        'id' => $model->getId(),
                        '_current' => true
                    ]);
                } else {
                    $this->_redirect('*/*/');
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', [
                    'id' => $this->getRequest()->getParam('id')
                ]);
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('testcasenews/news')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('News was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', ['id' => $id]);
            }
        }
        $this->_redirect('*/*/');
    }


    /* Mass actions */

    public function massDeleteAction()
    {
        $news = $this->getRequest()->getParam('news', null);

        if (is_array($news) && count($news) > 0) {
            try {
                foreach ($news as $id) {
                    Mage::getModel('testcasenews/news')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d news have been deleted', count($news)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select news'));
        }
        $this->_redirect('*/*');
    }

    public function massStatusAction()
    {
        $news = $this->getRequest()->getParam('news', null);
        // todo
        $status = intval($this->getRequest()->getParam('status', null));

        if (is_int($status) && is_array($news) && count($news) > 0) {
            try {
                foreach ($news as $id) {
                    $newsEntity = Mage::getModel('testcasenews/news')->load($id);
                    $newsEntity->setData('status', $status);
                    $newsEntity->save();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d news have been updated', count($news)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select news'));
        }
        $this->_redirect('*/*');
    }
}