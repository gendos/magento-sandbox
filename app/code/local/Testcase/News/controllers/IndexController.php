<?php
/**
 * Created by PhpStorm.
 * User: gendos
 * Date: 27.11.16
 * Time: 00:12
 */
class Testcase_News_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('News'));
        $this->renderLayout();
    }

    public function viewAction()
    {
        $newsId = Mage::app()->getRequest()->getParam('id', 0);
        $news = Mage::getModel('testcasenews/news')->load($newsId);

        if ($news->getId() > 0 && $news->getData('status') > 0) {
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle($news->getData('title').' - '.$this->__('News'));

            // add current news item title as last breadcrumb
            $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
            $breadcrumbs->addCrumb('news_item', [
                'label'=>$this->__($news->getData('title')),
                'title'=>$this->__($news->getData('title')),
            ]);

            $this->getLayout()->getBlock('news.content')->assign([
                "newsItem" => $news,
                "showDate" => Mage::helper('testcasenews/config')->isShowDateEnabled(),
            ]);
            $this->renderLayout();
        } else {
            $this->_forward('noRoute');
        }
    }
}