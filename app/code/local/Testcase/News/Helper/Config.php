<?php
/**
 * Created by PhpStorm.
 * User: gendos
 * Date: 27.11.16
 * Time: 23:52
 */

class Testcase_News_Helper_Config extends Mage_Core_Helper_Abstract
{
    /**
     * @return bool
     */
    public function isShowDateEnabled()
    {
        return ((int) Mage::getStoreConfig('testcasenews_config/news_options/show_date_created') === 1);
    }

    /**
     * @return int
     */
    public function getPreviewTextMaxLength()
    {
        return (int) Mage::getStoreConfig('testcasenews_config/news_options/preview_text_max_length') ?: 200;
    }

    /**
     * @return int
     */
    public function getPageNewsCount()
    {
        return (int) Mage::getStoreConfig('testcasenews_config/news_options/news_per_page') ?: 10;
    }
}